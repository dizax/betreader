# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
from sklearn.linear_model import LogisticRegression
import dyn_query as qry
import const
import sets
import time


class ModelBaseOptimizer:

    db_name = 'optimize_d.db'
    
    def __init__(self):
        # connect to db
        self.con_origin = mdb.connect('bet.db')
        self.cur_origin = self.con_origin.cursor()

        self.con = mdb.connect(self.db_name)
        self.cur = self.con.cursor()

        self.prev_seasons_cnt = const.model_prev_seasons_cnt
        #self.weeks_cnt = const.model_weeks_cnt
        self.weeks_cnt = 0

        # TODO cast group header names to inds
        self.sets = [qry.headers_inds(s_set, const.headers) for s_set in sets.sets]

        self.sets_cnt = 0

    def init(self):
        self.drop_tables()
        self.create_tables()
        self.populate_sets()

    def disconnect(self):
        self.cur.close()
        self.con.close()
        self.cur_origin.close()
        self.con_origin.close()

    def create_tables(self):
        sql = """CREATE TABLE IF NOT EXISTS Optimizer_list (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                country TEXT NOT NULL,
                season TEXT NOT NULL,
                week INTEGER NOT NULL,
                set_id INTEGER NOT NULL,
                result DOUBLE NOT NULL,
                res_str DOUBLE NOT NULL,
                guess DOUBLE NOT NULL,
                guess_str DOUBLE NOT NULL
            )"""
        self.cur.execute(sql)

        create_sql = qry.create_query(const.headers[8:] +
                                      [['w_window', 'INTEGER DEFAULT 0', 'd', 'update', 0, 1, 1]] +
                                      [['s_cnt', 'INTEGER DEFAULT 0', 'd', 'update', 0, 1, 1]]) % "Sets_list"
        self.cur.execute(create_sql)

    def drop_tables(self):
        self.cur.execute("DROP TABLE IF EXISTS Optimizer_list")
        self.cur.execute("DROP TABLE IF EXISTS Sets_list")

    def populate_sets(self):
        # TODO form sets groups
        m_sets = []
        for s_set in self.sets:
            # each set has to be modelled on 3 different data intervals
            for w_window in const.model_week_windows:
                for s_cnt in const.model_seasons_cnt:
                    m_sets.append([s_set, float(w_window), int(s_cnt)])
                    self.write_set(s_set, str(w_window), str(s_cnt))

        self.sets = m_sets

    def optimize_update_sets(self):
        # optimize new seasons for written sets
        # TODO
        return

    def optimize_add_sets(self):
        self.populate_sets()
        
        # already written sets cnt
        sql = "SELECT COUNT(*) FROM Sets_list"
        self.cur.execute(sql)
        self.sets_cnt = int(self.cur.fetchall()[0][0])

        # add new sets
        self.optimize_data()

    def optimize_new_data(self):
        # clear all
        self.init()

        # optimize all
        self.optimize_data()

    def optimize_data(self):
        print "optimizing data"
        sql = """SELECT country, league
            FROM Seasons_list
            WHERE country!='europe'
            GROUP BY country, league"""
        try:
            self.cur_origin.execute(sql)
            results = self.cur_origin.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')

                self.optimize_league_data(country, league)
        except mdb.Error:
            print "Seasons_list not exists"

    def optimize_league_data(self, country, league):
        for s_cnt, season in enumerate(self.seasons(country, league)):
            start_time = time.time()
            
            table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')
            print table_name

            weeks = self.weeks(table_name)
            for w_cnt in xrange(len(weeks)):
                print country, season, "week", w_cnt
                for s in xrange(len(self.sets)):
                    # all matches in given league since 'ssn_cnt'th previous season
                    # till 'week-weeks_cnt' week of current season
                    data, res = self.get_data(country, league, s_cnt, w_cnt, weeks, s)
                    model, ok = self.fit_data(data, res)

                    (result, res_str, guess, guess_str) = self.week_result(model, table_name, w_cnt, weeks, s) \
                        if ok else (0., 0., 0., 0.)

                    self.write_result(result, res_str, guess, guess_str, country, season, w_cnt, s+self.sets_cnt)
            print("- %s done in %s seconds -" % (table_name, time.time() - start_time))

    def seasons(self, country, league):
        sql = "SELECT season from Seasons_list WHERE country='%s' AND league='%s' ORDER BY season DESC" \
            % (country, league)
        self.cur_origin.execute(sql)
        res = self.cur_origin.fetchall()
        return [row[0].encode('utf-8') for row in res[1:]]  # ignore last season

    def weeks(self, table_name):
        sql = "SELECT COUNT(*) as cnt FROM %s" % ("stand_" + table_name)
        self.cur_origin.execute(sql)
        w_cnt = int(self.cur_origin.fetchall()[0][0]) / 2

        sql1 = "SELECT date FROM %s WHERE is_champ=1 ORDER BY date" % table_name
        self.cur_origin.execute(sql1)
        res = self.cur_origin.fetchall()

        # return first dates of match weeks
        return [row[0].encode('utf-8') for i, row in enumerate(res) if i % w_cnt == 0]

    def get_data(self, country, league, s_cnt, w_cnt, cur_weeks, s):
        # blah-blah-blah
        cur_week_min = cur_weeks[max(w_cnt - self.weeks_cnt, 0)]

        res = []
        data = []

        # select 'ssn_cnt' previous seasons
        sql1 = """
                SELECT country, league, season
                FROM Seasons_list s0
                WHERE season IN (
                    SELECT season
                    FROM Seasons_list s
                    WHERE s.country = s0.country
                    AND (SELECT COUNT(*)
                        FROM Seasons_list st
                        WHERE st.season >= s.season AND st.country = s.country) >= %d
                    AND (SELECT COUNT(*)
                        FROM Seasons_list st
                        WHERE st.season >= s.season AND st.country = s.country) < %d
                ) AND country='%s' AND league='%s';
                """ % (s_cnt+1, s_cnt+1+self.sets[s][2], country, league)

        try:
            self.cur_origin.execute(sql1)
            results1 = self.cur_origin.fetchall()

            for row1 in results1:
                self.get_row_data(data, res, row1, w_cnt, cur_week_min, s)
        except mdb.Error:
            print "Error: unable to fetch data for writing"
            exit(0)

        return data, res

    def get_row_data(self, data, res, row, w_cnt, cur_week_min, s):
        raise NotImplementedError("Subclass must implement abstract method")

    @staticmethod
    def fit_data(data, res):
        log_reg = LogisticRegression()
        try:
            log_reg.fit(data, res)
            ok = True
        except ValueError, e:
            print 'odni nuli/ebinitzi'
            #print e.message
            ok = False

        return log_reg, ok

    def week_result(self, model, table_name, w_cnt, weeks, s):
        raise NotImplementedError("Subclass must implement abstract method")

    def write_result(self, result, res_str, guess, guess_str, country, season, w_cnt, s):
        sql = """INSERT INTO Optimizer_list(country, season, week, set_id, result, res_str, guess, guess_str)
            VALUES ('%(country)s', '%(season)s', %(week)d, %(set_id)d, %(result)f, %(res_str)f, %(guess)f, %(guess_s)f)
            """ % {"country": country, "season": season, "week": w_cnt, "set_id": s+1,
                   "result": result, "res_str": res_str, "guess": guess, "guess_s": guess_str}
        try:
            self.cur.execute(sql)
            self.con.commit()
        except mdb.Error:
            print "cant add record to Optimizer_list"
            self.con.rollback()

    def write_set(self, s_set, w_window, s_cnt):
        # convert s_set inds to binary set
        binary_set = [str(int(i in s_set)) for i in xrange(8, len(const.headers))] + [w_window] + [s_cnt]
        sql = """INSERT INTO Sets_list(%s)
            VALUES (%s)
            """ % (','.join([h[0] for h in const.headers[8:]] + ['w_window'] + ['s_cnt']), ','.join(binary_set))
        try:
            self.cur.execute(sql)
            self.con.commit()
        except mdb.Error:
            print "cant add record to Sets_list"
            self.con.rollback()
