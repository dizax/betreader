# -*- coding: utf-8 -*-
__author__ = 'zax'

import time
import argparse
from bet_reader import BetReader
from backup import Backup
from database import Database
from db_processor import DbProcessor
from db_writer import DbWriter
from model import Model
from model_d import ModelDraw
from model_tri import ModelTri
import args_proc as a

parser = argparse.ArgumentParser()
parser.add_argument('--readLast', action='store_true', help="Read, save backup, process, write, model last seasons")
parser.add_argument('--readAll', action='store_true', help="Read, save backup, process, write, model all seasons, save backup")
parser.add_argument('--readAllBackup', action='store_true', help="Read all from backup, process, write, model all seasons")
parser.add_argument('--procLast', action='store_true', help="Process, write, model last seasons; ignore read")
parser.add_argument('--procAll', action='store_true', help="Process, write, model all seasons; ignore read")
parser.add_argument('--writeLast', action='store_true', help="Write last seasons; ignore read/process")
parser.add_argument('--writeAll', action='store_true', help="Write all seasons; ignore read/process")
parser.add_argument('--model', action='store_true', help="Model only; ignore read/process/write")

options = vars(parser.parse_args())
read, read_all, read_backup, process, proc_all, write, write_all, modeling = a.process_options(options)

if read_all:
    if not a.query_yes_no('This will drop the database. Are you sure?'):
        exit(0)

# init
db = Database(True, read_all)
reader = BetReader(read_all)
backup = Backup()
processor = DbProcessor(db, proc_all)
model = Model(db)
#model = ModelDraw(db)
#model = ModelTri(db)
writer = DbWriter(db, write_all)

print read, read_all, read_backup, process, proc_all, write, write_all, modeling

# write data to db
if read:
    start_time = time.time()
    if read_backup:
        backup.restore(db)
    else:
        reader.read(db)
        backup.save(db)
    print("--- reading done in %s seconds ---" % (time.time() - start_time))
#reader.close()
# process fetched data
if process:
    start_time = time.time()
    processor.process_raw_data()
    print("--- processing done in %s seconds ---" % (time.time() - start_time))
# write fetched data
if write:
    start_time = time.time()
    writer.print_to_xls()
    writer.print_to_txt()
    print("--- writing done in %s seconds ---" % (time.time() - start_time))
# fit data to model
if modeling:
    start_time = time.time()
    #model.fit_data()
    model.fit_seasons()  # model previous seasons
    print("--- modeling done in %s seconds ---" % (time.time() - start_time))

db.disconnect()
#backup.disconnect()

