# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
import dyn_query as qry
import const


class DbProcessor:

    def __init__(self, db, proc_all):
        self.con = db.con
        self.cur = db.cur

        self.proc_all = proc_all
        self.prev_ssn_cnt = const.prev_ssn_rate_cnt

        self.upd_d = qry.update_dict(const.headers)
        self.update_qry = qry.update_query(const.headers)

    @staticmethod
    def match_team(row, team):
        return int(row[0]) if row[1].encode('utf-8') == team else -int(row[0])

    def process_raw_data(self):
        print 'processing'

        # seasons list
        if self.proc_all:
            sql = """SELECT country, league, season
                FROM Seasons_list
                ORDER BY country, season"""
        else:
            sql = """SELECT country, league, max(season) as ssn
                FROM Seasons_list
                GROUP BY country, league"""

        try:
            # Execute the SQL command
            self.cur.execute(sql)
            # Fetch all the rows in a list of lists.
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')
                prev_table_names = self.prev_tables(self.prev_ssn_cnt, country, league, season)
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

                self.process_raw_season_standings(table_name)
                self.process_raw_season_stats(table_name, prev_table_names)
        except mdb.Error:
            print "Error: unable to fetch season"
            exit(0)

    def prev_tables(self, cnt, country, league, season):
        sql = """SELECT season
                FROM Seasons_list
                WHERE country='%s' AND league='%s' AND season<'%s'
                ORDER BY season DESC
                LIMIT %d
                """ % (country, league, season, cnt)

        try:
            self.cur.execute(sql)

            res = self.cur.fetchall()
            return [country + '_' + league.replace('-', '') + '_' + (ssn[0].encode('utf-8').replace('-', '')
                                                                     if len(res) > 0
                                                                     else '')
                    for ssn in res]
        except mdb.Error:
            print "Error: unable to fetch prev_table_names"
            return []

    def process_raw_season_standings(self, table_name):
        print "standings " + table_name

        # create new table
        self.cur.execute("DROP TABLE IF EXISTS " + "stand_" + table_name)

        # create table (if don't exist?)
        sql = """CREATE TABLE IF NOT EXISTS %s (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                team_name TEXT DEFAULT NULL,
                points_cnt INTEGER DEFAULT 0,
                matches_cnt INTEGER DEFAULT 0
            )""" % ("stand_"+table_name)

        self.cur.execute(sql)

        # compute standings
        sql1 = """INSERT INTO %(stand)s (team_name, points_cnt, matches_cnt)
            SELECT teamID, SUM(MatchResult) AS points_cnt, COUNT(*) AS matches_cnt
            FROM
            (
                SELECT team1 AS teamID,
                CASE WHEN goals1 > goals2 THEN 3
                    WHEN goals1 < goals2 THEN 0
                    WHEN goals1 = goals2 THEN 1
                END AS MatchResult
                FROM %(table)s
                WHERE is_champ=1

                UNION ALL
                SELECT team2 AS teamID,
                CASE WHEN goals1 < goals2 THEN 3
                    WHEN goals1 > goals2 THEN 0
                    WHEN goals1 = goals2 THEN 1
                END AS MatchResult
                FROM %(table)s
                WHERE is_champ=1
            ) A
            GROUP BY teamID
            ORDER BY points_cnt DESC
            """ % {"stand": "stand_"+table_name, "table": table_name}

        try:
            self.cur.execute(sql1)
        except mdb.Error:
            print "Error: unable to compute standings"
            exit(0)

    def process_raw_season_stats(self, table_name, prev_table_names):
        print "stats " + table_name

        sql_select = """SELECT date, team1, team2, is_champ
            FROM %s ORDER BY date""" % table_name
        try:
            self.cur.execute(sql_select)

            results = self.cur.fetchall()

            for row in results:
                #date = row[0].strftime('%Y-%m-%d')
                date = row[0].encode('utf-8')
                team1 = row[1].encode('utf-8')
                team2 = row[2].encode('utf-8')
                is_champ = int(row[3])

                self.fill_match_stats(table_name, prev_table_names, date, team1, team2, is_champ)
        except mdb.Error:
            print "Error: unable to fetch date,team1,team2"
            exit(0)

    def fill_match_stats(self, table_name, prev_table_names, date, team1, team2, is_champ):
        # create update dict
        upd_d = self.upd_d

        # previous championship matches
        sql1 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team1, team1, date)

        sql2 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team2, team2, date)

        try:
            self.cur.execute(sql1)
            results1 = [self.match_team(row, team1) for row in self.cur.fetchall()]

            self.cur.execute(sql2)
            results2 = [self.match_team(row, team2) for row in self.cur.fetchall()]

            upd_d['prev1_1'] = results1[0] if len(results1) > 0 else 0
            upd_d['prev1_2'] = results2[0] if len(results2) > 0 else 0

            upd_d['prev3_1'] = sum([results1[i] for i in xrange(min(3, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prev3_2'] = sum([results2[i] for i in xrange(min(3, len(results2)))]) if len(results2) > 0 else 0
            
            upd_d['prev7_1'] = sum([results1[i] for i in xrange(min(7, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prev7_2'] = sum([results2[i] for i in xrange(min(7, len(results2)))]) if len(results2) > 0 else 0

            upd_d['prev15_1'] = sum(results1) if len(results1) > 0 else 0
            upd_d['prev15_2'] = sum(results2) if len(results2) > 0 else 0
        except mdb.Error:
            print "Error: unable to fetch previous"
            exit(0)

        # previous all matches
        sql1 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s'
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team1, team1, date)

        sql2 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s'
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team2, team2, date)

        try:
            self.cur.execute(sql1)
            results1 = [self.match_team(row, team1) for row in self.cur.fetchall()]

            self.cur.execute(sql2)
            results2 = [self.match_team(row, team2) for row in self.cur.fetchall()]

            upd_d['prevAll1_1'] = results1[0] if len(results1) > 0 else 0
            upd_d['prevAll1_2'] = results2[0] if len(results2) > 0 else 0

            upd_d['prevAll3_1'] = sum([results1[i] for i in xrange(min(3, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prevAll3_2'] = sum([results2[i] for i in xrange(min(3, len(results2)))]) if len(results2) > 0 else 0
            
            upd_d['prevAll7_1'] = sum([results1[i] for i in xrange(min(7, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prevAll7_2'] = sum([results2[i] for i in xrange(min(7, len(results2)))]) if len(results2) > 0 else 0

            upd_d['prevAll15_1'] = sum(results1) if len(results1) > 0 else 0
            upd_d['prevAll15_2'] = sum(results2) if len(results2) > 0 else 0
        except mdb.Error:
            print "Error: unable to fetch previous"
            exit(0)

        # previous home/away championship matches
        sql1 = """SELECT result, cof1
            FROM %s
            WHERE team1='%s' AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team1, date)

        sql2 = """SELECT result, cof2
            FROM %s
            WHERE team2='%s' AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team2, date)

        try:
            self.cur.execute(sql1)
            res = self.cur.fetchall()
            results1 = [int(s[0]) for s in res]
            cof1 = res[0][1] if len(res) > 0 else 0.

            self.cur.execute(sql2)
            res = self.cur.fetchall()
            results2 = [-int(s[0]) for s in res]
            cof2 = res[0][1] if len(res) > 0 else 0.

            upd_d['prevHA1_1'] = results1[0] if len(results1) > 0 else 0
            upd_d['prevHA1_2'] = results2[0] if len(results2) > 0 else 0

            upd_d['prevHA3_1'] = sum([results1[i] for i in xrange(min(3, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prevHA3_2'] = sum([results2[i] for i in xrange(min(3, len(results2)))]) if len(results2) > 0 else 0
            
            upd_d['prevHA7_1'] = sum([results1[i] for i in xrange(min(7, len(results1)))]) if len(results1) > 0 else 0
            upd_d['prevHA7_2'] = sum([results2[i] for i in xrange(min(7, len(results2)))]) if len(results2) > 0 else 0

            upd_d['prevHA15_1'] = sum(results1) if len(results1) > 0 else 0
            upd_d['prevHA15_2'] = sum(results2) if len(results2) > 0 else 0

            upd_d['prevHAWinCof1'] = float(upd_d['prevHA1_1']) * cof1
            upd_d['prevHAWinCof2'] = float(upd_d['prevHA1_2']) * cof2
        except mdb.Error:
            print "Error: unable to fetch previousHA"
            exit(0)

        # previous draws cnt (can be united with prev championship mathches)
        sql1 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team1, team1, date)

        sql2 = """SELECT result, team1, team2
            FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND is_champ=1
            ORDER BY date DESC
            LIMIT 15""" % (table_name, team2, team2, date)

        try:
            self.cur.execute(sql1)
            results1 = [self.match_team(row, team1) for row in self.cur.fetchall()]

            self.cur.execute(sql2)
            results2 = [self.match_team(row, team2) for row in self.cur.fetchall()]

            upd_d['draw3_1'] = len([results1[i] for i in xrange(min(3, len(results1))) if results1[i] == 0])
            upd_d['draw3_2'] = len([results2[i] for i in xrange(min(3, len(results2))) if results2[i] == 0])
            
            upd_d['draw7_1'] = len([results1[i] for i in xrange(min(7, len(results1))) if results1[i] == 0])
            upd_d['draw7_2'] = len([results2[i] for i in xrange(min(7, len(results2))) if results2[i] == 0])

            upd_d['draw15_1'] = len([r for r in results1 if r == 0]) if len(results1) > 0 else 0
            upd_d['draw15_2'] = len([r for r in results2 if r == 0]) if len(results2) > 0 else 0
        except mdb.Error:
            print "Error: unable to fetch previous"
            exit(0)
        
        # previous any 3 matches (T1vsT2 or T2vsT1) (including previous seasons) (champ only)
        if len(prev_table_names) > 0:
            sqls = []
            for table in [table_name]+prev_table_names:
                sqls.append("""SELECT result, team1, team2
                    FROM %s
                    WHERE ((team1='%s' AND team2='%s') OR (team1='%s' AND team2='%s')) AND date < '%s' AND is_champ=1
                    ORDER BY date DESC
                    """ % (table, team1, team2, team2, team1, date)
                )

            try:
                results = []
                for sql in sqls:
                    self.cur.execute(sql)

                    res = self.cur.fetchall()
                    results += [self.match_team(r, team1) for r in res]
                    if len(results) > 5:
                        break

                upd_d['prevAny3'] = sum([results[i] for i in xrange(min(3, len(results)))])
                upd_d['prevAny5'] = sum([results[i] for i in xrange(min(5, len(results)))])
            except mdb.Error:
                print "Error: unable to fetch prevSeason"
                exit(0)
        else:
            upd_d['prevAny3'] = 0

        # previous season match
        if len(prev_table_names) > 0:
            sql = """SELECT result
                FROM %s
                WHERE team1='%s' AND team2='%s'
                """ % (prev_table_names[0], team1, team2)

            try:
                self.cur.execute(sql)

                res = self.cur.fetchall()
                upd_d['prevSSn'] = int(res[0][0]) if len(res) > 0 else 0
            except mdb.Error:
                print "Error: unable to fetch prevSeason"
                exit(0)
        else:
            upd_d['prevSSn'] = 0

        # previous seasons rating/standings
        upd_d['team1_rate5'] = 0
        upd_d['team2_rate5'] = 0

        for ti, t_name in enumerate(prev_table_names):
            sql = """SELECT id FROM %s
                """ % ("stand_" + t_name)
            sql1 = """SELECT id FROM %s
                WHERE team_name='%s'
                """ % ("stand_" + t_name, team1)
            sql2 = """SELECT id FROM %s
                WHERE team_name='%s'
                """ % ("stand_" + t_name, team2)

            try:
                self.cur.execute(sql)
                teams_cnt = len(self.cur.fetchall())

                self.cur.execute(sql1)
                res = self.cur.fetchall()
                if ti == 0: upd_d['team1_rate1'] = (int(res[0][0]) if len(res) > 0 else teams_cnt)
                upd_d['team1_rate5'] += (int(res[0][0]) if len(res) > 0 else teams_cnt)

                self.cur.execute(sql2)
                res = self.cur.fetchall()
                if ti == 0: upd_d['team2_rate1'] = (int(res[0][0]) if len(res) > 0 else teams_cnt)
                upd_d['team2_rate5'] += (int(res[0][0]) if len(res) > 0 else teams_cnt)
            except mdb.Error:
                print "Error: unable to fetch team standings"
                exit(0)

        if len(prev_table_names) == 0:
            sql = """SELECT id FROM %s
                """ % ("stand_" + table_name)
            try:
                self.cur.execute(sql)
                teams_cnt = len(self.cur.fetchall())

                upd_d['team1_rate1'] = teams_cnt
                upd_d['team2_rate1'] = teams_cnt
                upd_d['team1_rate5'] = teams_cnt
                upd_d['team2_rate5'] = teams_cnt
            except mdb.Error:
                print "Error: unable to fetch teams_cnt"
                exit(0)
        else:
            upd_d['team1_rate5'] /= float(len(prev_table_names))
            upd_d['team2_rate5'] /= float(len(prev_table_names))

        # matches played in last N days
        sqls1 = ["""SELECT '1' FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND JULIANDAY('%s')-JULIANDAY(date) < %d
            """ % (table_name, team1, team1, date, date, n) for n in [15, 30, 60]]

        sqls2 = ["""SELECT '1' FROM %s
            WHERE (team1='%s' OR team2='%s') AND date < '%s' AND JULIANDAY('%s')-JULIANDAY(date) < %d
            """ % (table_name, team2, team2, date, date, n) for n in [15, 30, 60]]

        try:
            results = []
            for sql1, sql2 in zip(sqls1, sqls2):
                self.cur.execute(sql1)
                results1 = len(self.cur.fetchall())
                self.cur.execute(sql2)
                results2 = len(self.cur.fetchall())
                results.append([results1, results2])

            upd_d['gcnt15_1'] = results[0][0]
            upd_d['gcnt15_2'] = results[0][1]
            upd_d['gcnt30_1'] = results[1][0]
            upd_d['gcnt30_2'] = results[1][1]
            upd_d['gcnt60_1'] = results[2][0]
            upd_d['gcnt60_2'] = results[2][1]
        except mdb.Error:
            print "Error: unable to fetch previous matches in period cnt"
            exit(0)

        # Anton's parameters
        upd_d['team1_rate3'] = upd_d['team1_rate5']+upd_d['team1_rate1']
        upd_d['team2_rate3'] = upd_d['team2_rate5']+upd_d['team2_rate1']
        upd_d['t_r'] = upd_d['team1_rate5']/upd_d['team2_rate5']
        upd_d['sqtr'] = (abs(upd_d['team1_rate5']-upd_d['team2_rate5']))**0.5*(upd_d['team1_rate5']-upd_d['team2_rate5'])/(abs(upd_d['team1_rate5']-upd_d['team2_rate5'])+0.01)
        upd_d['sqpr7'] = (abs(upd_d['prev7_1']-upd_d['prev7_2']))**0.5*(upd_d['prev7_1']-upd_d['prev7_2'])/(abs(upd_d['prev7_1']-upd_d['prev7_2'])+0.01)
        upd_d['sqpr15'] = (abs(upd_d['prev15_1']-upd_d['prev15_2']))**0.5*(upd_d['prev15_1']-upd_d['prev15_2'])/(abs(upd_d['prev15_1']-upd_d['prev15_2'])+0.01)
        upd_d['sqpra7'] = (abs(upd_d['prevAll7_1']-upd_d['prevAll7_2']))**0.5*(upd_d['prevAll7_1']-upd_d['prevAll7_2'])/(abs(upd_d['prevAll7_1']-upd_d['prevAll7_2'])+0.01)
        upd_d['sqpra15'] = (abs(upd_d['prevAll15_1']-upd_d['prevAll15_2']))**0.5*(upd_d['prevAll15_1']-upd_d['prevAll15_2'])/(abs(upd_d['prevAll15_1']-upd_d['prevAll15_2'])+0.01)
        upd_d['sqprha7'] = (abs(upd_d['prevHA7_1']-upd_d['prevHA7_2']))**0.5*(upd_d['prevHA7_1']-upd_d['prevHA7_2'])/(abs(upd_d['prevHA7_1']-upd_d['prevHA7_2'])+0.01)
        upd_d['sqprha15'] = (abs(upd_d['prevHA15_1']-upd_d['prevHA15_2']))**0.5*(upd_d['prevHA15_1']-upd_d['prevHA15_2'])/(abs(upd_d['prevHA15_1']-upd_d['prevHA15_2'])+0.01)

        # update
        upd_d['tableName'] = table_name
        upd_d['date'] = date
        upd_d['team1'] = team1
        upd_d['team2'] = team2

        update_sql = self.update_qry % upd_d

        try:
            # Execute the SQL command
            self.cur.execute(update_sql)
            self.con.commit()
        except mdb.Error:
            print "Error: unable to update data"
            exit(0)
