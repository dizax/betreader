# -*- coding: utf-8 -*-
__author__ = 'zax'

from datetime import datetime
try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
from model_base import ModelBase
import dyn_query as qry
import const


class ModelDraw(ModelBase):

    db_name = 'optimize_d.db'
    xls_name = 'model_d'

    def fit_league_data(self, color, ws, table_name, country, league, season, mw_cnt):
        headers = ['date', 'team1', 'team2', 'goals1', 'goals2', 'result', 'c_cofX', 'c_cofnX',
                   'set_id', 'm_cofX', 'm_cofnX', 'EX', 'EnX']
        for c, h in enumerate(headers):
            ws.write(0, c, h.decode('utf-8'))

        # first dates of match weeks
        weeks = self.weeks(table_name)

        # predict week results
        profit, profit_str, guess, guess_str, bet, bet_str, r_cnt = 0., 0., 0, 0, 0, 0, 0
        w_profit = []
        for w_cnt, (week, week_next) in reversed(list(enumerate(zip(weeks, weeks[1:] + [None])))):
            m_profit, m_profit_str, m_guess, m_guess_str, m_rcnt = self.fit_week_data(color, ws, r_cnt, table_name,
                                                                                      country, league, season, mw_cnt,
                                                                                      w_cnt, week, week_next)
            profit += m_profit
            profit_str += m_profit_str
            guess += m_guess
            guess_str += m_guess_str
            bet += m_rcnt[0]
            bet_str += m_rcnt[1]
            r_cnt += m_rcnt[2]

            w_profit.append(m_profit)

        # cumulative w_profits
        w_profit = [sum(w_profit[i:]) for i, wp in reversed(list(enumerate(w_profit)))]

        # write profit results
        ws.write(0, len(headers)+4, "profit")
        ws.write(1, len(headers)+4, profit)
        ws.write(0, len(headers)+5, "profit_str")
        ws.write(1, len(headers)+5, profit_str)
        ws.write(0, len(headers)+6, "guess")
        ws.write(1, len(headers)+6, float(guess)/float(bet) if bet != 0 else 0.)
        ws.write(0, len(headers)+7, "guess_str")
        ws.write(1, len(headers)+7, float(guess_str)/float(bet_str) if bet_str != 0 else 0.)
        ws.write(2, len(headers)+6, "bet_cnt")
        ws.write(3, len(headers)+6, bet)
        ws.write(2, len(headers)+7, "bet_str_cnt")
        ws.write(3, len(headers)+7, bet_str)

        return profit, w_profit

    def predict_result(self, table_name, week, week_next, best_set_id, best_set, log_reg, ws, r_cnt, color):
        # games list
        if week_next is not None:
            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, week, week_next)
        else:
            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, week)

        # predict
        try:
            self.db.cur.execute(sql)
            results = self.db.cur.fetchall()

            profit, profit_str, guess, guess_str, bet, bet_str = 0., 0., 0, 0, 0, 0
            for r, row in enumerate(results):
                #date = row[2].strftime('%Y-%m-%d')
                date = row[2].encode('utf-8')
                c1 = float(row[9])
                cx = float(row[10])
                c2 = float(row[11])

                data_to_write = [
                    row[2],  # 0 date
                    row[3],  # 1 team1
                    row[4],  # 2 team2
                    int(row[5]),  # 3 goals1
                    int(row[6]),  # 4 goals2
                    int(row[7]),  # 5 result
                    cx,  # 6 c_cofX
                    1./(1./c1+1./c2),  # 7 c_cofnX
                    best_set_id  # 8 set_id
                ]

                test_data = [qry.format_row_xls(row, best_set[:-2], const.headers)]

                #results = log_reg.predict(test_data)
                probs = log_reg.predict_proba(test_data)

                try:
                    data_to_write.append(probs[0][0])  # 9 m_cofX
                    data_to_write.append(probs[0][1])  # 10 m_cofnX
                    data_to_write.append(probs[0][0]*data_to_write[6])  # 11 EX = c_cofX*m_cofX
                    data_to_write.append(probs[0][1]*data_to_write[7])  # 12 EnX = c_cofnX*m_cofnX

                    e = data_to_write[11:]
                    e_bet = e.index(max(e))
                    s = [probs[0][0], probs[0][1]]
                    s_bet = s.index(max(s))
                    data_to_write.append(["draw", "none"][e_bet])

                    data_to_write.append(cx-1. if int(row[7]) == e_bet == 0 else
                                         (-1. if int(row[7]) != 0 and e_bet == 0 else 0.))  # profit m_cof*E
                    data_to_write.append(cx-1. if int(row[7]) == s_bet == 0 else
                                         (-1. if int(row[7]) != 0 and s_bet == 0 else 0.))  # profit straight
                except IndexError:
                    continue

                if (datetime.now() - datetime.strptime(date, '%Y-%m-%d')).days < 1:
                    for c, col in enumerate(data_to_write):
                        ws.write(r_cnt + r+1, c, col, color[0])
                else:
                    if e_bet == 0:
                        for c, col in enumerate(data_to_write):
                            ws.write(r_cnt + r+1, c, col, color[1])
                    else:
                        for c, col in enumerate(data_to_write):
                            ws.write(r_cnt + r+1, c, col)
                    profit += data_to_write[-2]
                    profit_str += data_to_write[-1]
                    guess += int(int(row[7]) == e_bet == 0)
                    guess_str += int(int(row[7]) == s_bet == 0)
                    bet += int(e_bet == 0)
                    bet_str += int(s_bet == 0)

            return profit, profit_str, guess, guess_str, (bet, bet_str, len(results))
        except mdb.Error:
            print "Error: unable to fetch data for modeling"
            exit(0)

    def get_row_data(self, data, res, row, w_cnt, date, best_set):
        country = row[0].encode('utf-8')
        league = row[1].encode('utf-8')
        season = row[2].encode('utf-8')
        table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

        # -- data interval ---------------------------------
        weeks = self.weeks(table_name)
        week_window = (qry.odd(int(len(weeks) / best_set[-2])) - 1) / 2

        week_min_i = max(w_cnt - week_window, 0)
        if week_min_i + 2*week_window < len(weeks):
            week_max_i = week_min_i + 2*week_window
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, date, weeks[week_min_i], weeks[week_max_i])
        else:
            week_min_i = max(len(weeks)-1 - 2*week_window, 0)
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, date, weeks[week_min_i])
        # -- data interval ---------------------------------

        self.db.cur.execute(sql)
        results = self.db.cur.fetchall()

        for row in results:
            res.append(int(int(row[7]) != 0))
            data.append(qry.format_row_xls(row, best_set[:-2], const.headers))
