# -*- coding: utf-8 -*-
__author__ = 'zax'

import os
try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
import xlsxwriter
import dyn_query as qry
import const


class DbWriter:

    def __init__(self, db, write_all):
        self.con = db.con
        self.cur = db.cur

        self.write_all = write_all

        self.inds = qry.headers_inds(const.headers_to_write, const.headers)
        self.txt_formats = qry.formats_to_write(self.inds, const.headers)

    @staticmethod
    def new_dir(m_name):
        if not os.path.exists(m_name):
            os.makedirs(m_name)
        os.chdir(m_name)

    def print_to_txt(self):
        print 'Writing to txt'

        self.new_dir("betTXT")

        # seasons list
        if self.write_all:
            sql = """SELECT country, league, season
                FROM Seasons_list
                ORDER BY country, season DESC"""
        else:
            sql = """SELECT country, league, max(season) as ssn
                FROM Seasons_list
                GROUP BY country, league"""

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

                self.new_dir(country)
                self.print_season_to_txt(table_name)
                os.chdir('..')
        except mdb.Error:
            print "Error: unable to fetch data for writing txt 1"
            exit(0)

        os.chdir('..')

    def print_season_to_txt(self, table_name):
        bet_file = open('%s.txt' % table_name, 'w')

        bet_file.write("\t".join(const.headers_to_write) + "\n")

        # games list
        sql = """SELECT * FROM %s
            WHERE is_champ=1
            ORDER BY date DESC""" % table_name

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                format_row = tuple(qry.format_row_txt(row, self.inds, const.headers))

                bet_file.write(self.txt_formats % format_row)
        except mdb.Error:
            print "Error: unable to fetch data for writing txt 2"
            exit(0)

        bet_file.close()

    def print_to_xls(self):
        print 'Writing to xls'

        # seasons list
        if self.write_all:
            self.print_all_to_xls()
        else:
            self.print_last_season_to_xls()

    def print_last_season_to_xls(self):
        sql = """SELECT country, league, max(season) as ssn
                FROM Seasons_list
                GROUP BY country, league"""

        wb = xlsxwriter.Workbook('last_res.xlsx')
        #date_f = wb.add_format({'num_format': 'd-m-yyyy'})

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')
                ws = wb.add_worksheet(table_name)

                self.print_league_season_to_xls(ws, table_name)
        except mdb.Error:
            print "Error: unable to fetch data for writing last xls"
            exit(0)

        wb.close()

    def print_all_to_xls(self):
        self.new_dir("betXLS")
        
        sql = """SELECT country
                FROM Seasons_list
                WHERE country!='europe'
                ORDER BY country"""
        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                self.print_league_to_xls(country)
        except mdb.Error:
            print "Error: unable to fetch data for writing all xls"
            exit(0)
        
        os.chdir('..')

    def print_league_to_xls(self, country):
        sql = """SELECT league, season
                FROM Seasons_list
                WHERE country='%s'
                ORDER BY season DESC""" % country

        wb = xlsxwriter.Workbook('%s.xlsx' % country)
        #date_f = wb.add_format({'num_format': 'd-m-yyyy'})

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                league = row[0].encode('utf-8')
                season = row[1].encode('utf-8')
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')
                ws = wb.add_worksheet(season)

                self.print_league_season_to_xls(ws, table_name)
        except mdb.Error:
            print "Error: unable to fetch data for writing league xls"
            exit(0)

        wb.close()

    def print_league_season_to_xls(self, ws, table_name):
        for c, h in enumerate(const.headers_to_write):
            ws.write(0, c, h.decode('utf-8'))

        # games list
        sql = """SELECT * FROM %s
            WHERE is_champ=1
            ORDER BY date, id DESC""" % table_name

        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for r, row in enumerate(results):
                format_row = qry.format_row_xls(row, self.inds, const.headers)
                for c, col in enumerate(format_row):
                    ws.write(r+1, c, col)

        except mdb.Error:
            print "Error: unable to fetch data for writing xls 1"
            exit(0)
