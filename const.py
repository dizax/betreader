# -*- coding: utf-8 -*-
__author__ = 'zax'

import convert as cf

LEAGUES = [
   ['europe', 'champions-league'],
    ['europe', 'europa-league'],
    ['england', 'premier-league'],
    ['spain', 'primera-division'],
    ['germany', 'bundesliga'],
    ['italy', 'serie-a'],
    ['france', 'ligue-1'],
    ['russia', 'premier-league'],
    ['turkey', 'super-lig']
]

SUB_LEAGUES = {
    'england': ['fa-cup', 'capital-one-cup'],
    'spain': ['copa-del-rey'],
    'germany': ['dfb-pokal'],
    'italy': ['coppa-italia'],
    'france': ['coupe-de-france'],
    'russia': ['russian-cup'],
    'turkey': ['turkish-cup']
}

DB_HOST = 'localhost'
DB_USERNAME = 'betuser'
DB_PASSWORD = 'bet'
DB_NAME = 'betdb'

prev_ssn_rate_cnt = 5
model_prev_seasons_cnt = 7
model_seasons_cnt = [8]
model_weeks_cnt = [1, 2, 3, 4]

model_seasons_to_fit = ['2014-2015']
model_week_windows = [1, 2, 3, 4, 5]

headers_to_write = [
    'date', 'team1', 'team2', 'goals1', 'goals2', 'result', 'result01', 'cof1', 'cofX', 'cof2',
    'team1_rate1', 'team2_rate1', 'team1_rate5', 'team2_rate5',
    'prev1_1', 'prev1_2', 'prev3_1', 'prev3_2', 'prev7_1', 'prev7_2', 'prev15_1', 'prev15_2',
    'prevAll1_1', 'prevAll1_2', 'prevAll3_1', 'prevAll3_2', 'prevAll7_1', 'prevAll7_2', 'prevAll15_1', 'prevAll15_2',
    'prevHA1_1', 'prevHA1_2', 'prevHA3_1', 'prevHA3_2', 'prevHA7_1', 'prevHA7_2', 'prevHA15_1', 'prevHA15_2',
    'draw3_1', 'draw3_2', 'draw7_1', 'draw7_2', 'draw15_1', 'draw15_2',
    'gcnt15_1', 'gcnt15_2', 'gcnt30_1', 'gcnt30_2', 'gcnt60_1', 'gcnt60_2',
    'prevAny3', 'prevAny5', 'prevSSn', 'prevHAWinCof1', 'prevHAWinCof2',
   'sqtr', 't_r', 'sqpr7', 'sqpr15', 'sqpra7', 'sqpra15', 'sqprha7', 'sqprha15', 'p1','pX', 'p2'
]


# [0,  , 1      , 2       , 3          , 4      , 5            , 6            ]
# [name, db_type, txt_type, read/update, def_val, txt_convert_f, xls_convert_f]
headers = [
    ['is_champ', 'INTEGER NOT NULL', 'd', 'read', 0, cf.cint, cf.cint],
    ['date', 'DATE NOT NULL', 's', 'read', '', cf.date, cf.date],
    ['team1', 'TEXT NOT NULL', 's', 'read', '', cf.encode, cf.raw],
    ['team2', 'TEXT NOT NULL', 's', 'read', '', cf.encode, cf.raw],
    ['goals1', 'INTEGER NOT NULL', 'd', 'read', 0, cf.cint, cf.cint],
    ['goals2', 'INTEGER NOT NULL', 'd', 'read', 0, cf.cint, cf.cint],
    ['result', 'INTEGER NOT NULL', 'd', 'read', 0, cf.cint, cf.cint],
    ['result01', 'INTEGER NOT NULL', 'd', 'read', 0, cf.cint, cf.cint],
    ['cof1', 'DOUBLE', 'f', 'read', 0., cf.cfloat, cf.cfloat],
    ['cofX', 'DOUBLE', 'f', 'read', 0., cf.cfloat, cf.cfloat],
    ['cof2', 'DOUBLE', 'f', 'read', 0., cf.cfloat, cf.cfloat],
    ['team1_rate1', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['team2_rate1', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['team1_rate5', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['team2_rate5', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['prev1_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev1_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev3_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev3_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev7_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev7_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev15_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prev15_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll1_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll1_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll3_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll3_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll7_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll7_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll15_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAll15_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA1_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA1_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA3_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA3_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA7_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA7_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA15_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHA15_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreak_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreak_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreakAll_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreakAll_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreakHA_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['WLstreakHA_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreak_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreak_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreakAll_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreakAll_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreakHA_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['nWnLstreakHA_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw3_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw3_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw7_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw7_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw15_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['draw15_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['Dstreak_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['Dstreak_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['DstreakAll_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['DstreakAll_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['DstreakHA_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    #['DstreakHA_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt15_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt15_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt30_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt30_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt60_1', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['gcnt60_2', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAny3', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevAny5', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevSSn', 'INTEGER DEFAULT 0', 'd', 'update', 0, cf.cint, cf.cint],
    ['prevHAWinCof1', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['prevHAWinCof2', 'DOUBLE DEFAULT 0.0', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    #'team1_rate3','team2_rate3', 'sqtr', 't_r', 'sqpr7', 'sqpr15', 'sqpra7', 'sqpra15', 'sqprha7', 'sqprha15', 'p1','pX', 'p2'
    ['p1', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['pX', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['p2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    #['team1_rate3', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    #['team2_rate3', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqtr', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['t_r', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqpr7', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqpr15', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqpra7', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqpra15', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqprha7', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['sqprha15', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['tr2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['pr7_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['pr15_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['pra7_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['pra15_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['prha7_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
    ['prha15_2', 'DOUBLE', 'f', 'update', 0., cf.cfloat, cf.cfloat],
]
