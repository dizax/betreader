# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
import const
import dyn_query as qry


class Backup:

    m_cur_table = ''

    def __init__(self):
        # connect to db
        self.con = mdb.connect('bet_backup.db')
        self.cur = self.con.cursor()

        self.select_qry = qry.select_query([prop for prop in const.headers if 'read' in prop[3]])
        self.insert_qry = qry.insert_many_query([prop for prop in const.headers if 'read' in prop[3]])

    def drop_seasons(self):
        sql = """SELECT country, league, season
            FROM Seasons_list"""
        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

                self.drop_season(table_name)
        except mdb.Error:
            #print "No backup yet"
            return -1

        self.cur.execute("DROP TABLE IF EXISTS Seasons_list")
        return 1

    def drop_season(self, table_name):
        self.cur.execute("DROP TABLE IF EXISTS %s" % table_name)

    def disconnect(self):
        self.cur.close()
        self.con.close()
        return

    def save(self, db):
        print 'Dropping backup'
        if not self.drop_seasons():
            print "No backup yet"

        self.from_to(db.con, db.cur, self.con, self.cur)

    def restore(self, db):
        print 'Dropping db'
        if not db.drop_seasons():
            print "No db yet"

        self.from_to(self.con, self.cur, db.con, db.cur)

    def from_to(self, con_from, cur_from, con_to, cur_to):
        # create Seasons_list
        sql = """CREATE TABLE IF NOT EXISTS Seasons_list (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                country TEXT NOT NULL,
                league TEXT NOT NULL,
                season TEXT NOT NULL
            )"""
        cur_to.execute(sql)

        # populate seasons list
        sql = """SELECT country, league, season
            FROM Seasons_list"""
        try:
            cur_from.execute(sql)
            results = cur_from.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')

                self.add_season(country, league, season, con_from, cur_from, con_to, cur_to)
        except mdb.Error:
            print "???"
            exit(0)

    def add_season(self, country, league, season, con_from, cur_from, con_to, cur_to):
        # add season to seasons list
        sql = """INSERT INTO Seasons_list(country, league, season)
            VALUES ('%(country)s', '%(league)s', '%(season)s')
            """ % {"country": country, "league": league, "season": season}
        try:
            cur_to.execute(sql)
            con_to.commit()
        except mdb.Error:
            print "cant add record to seasons table"
            con_to.rollback()

        # create new table
        table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')
        try:
            cur_to.execute("DROP TABLE IF EXISTS " + table_name)
            # create table
            create_sql = qry.create_query(const.headers) % table_name
            cur_to.execute(create_sql)
        except mdb.Error:
            print "cant create season table"
        
        # copy data (ignore update columns)
        try:
            sql_read = self.select_qry % table_name
            cur_from.execute(sql_read)
            results = cur_from.fetchall()
        except mdb.Error:
            print "cant read all from season table"
            return

        try:
            insert_qry = self.insert_qry % {"tableName": table_name}
            cur_to.executemany(insert_qry, results)
            con_to.commit()
        except mdb.Error:
            print "cant write to season table"
