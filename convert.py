# -*- coding: utf-8 -*-
__author__ = 'zax'


def raw(val):
    return val


def encode(val):
    return val.encode('utf-8')


def cint(val):
    return int(val)


def cfloat(val):
    return float(val)


def date(val):
    return val.encode('utf-8')
    #return val.strftime('%Y-%m-%d')
