# -*- coding: utf-8 -*-
__author__ = 'zax'


def create_query(props):
    s0 = "CREATE TABLE %s (\nid INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n"
    s1 = ",\n".join(["%s %s" % (prop[0], prop[1]) for prop in props])
    s2 = '\n)'

    return s0+s1+s2


def select_query(props):
    s0 = "SELECT "
    s1 = ", ".join(["%s" % prop[0] for prop in props])
    s2 = "\nFROM %s "
    s3 = "ORDER BY date DESC"

    return s0+s1+s2+s3


def insert_many_query(props):
    s0 = "INSERT INTO %(tableName)s ("
    s1 = ", ".join(["%s" % prop[0] for prop in props])
    s2 = ")\nVALUES ("
    s3 = ", ".join(["?" for prop in props])
    s4 = ")"

    return s0+s1+s2+s3+s4


def insert_query(props):
    s0 = "INSERT INTO %(tableName)s ("
    s1 = ", ".join(["%s" % prop[0] for prop in props])
    s2 = ")\nVALUES ("
    s3 = ", ".join(["'%({name}){format}'".format(name=prop[0], format=prop[2])
                    for prop in props])
    s4 = ")"

    return s0+s1+s2+s3+s4


def insert_dict(props):
    return {prop[0]: prop[4] for prop in props if 'read' in prop[3]}


def insert_eu_query(props):
    s0 = "INSERT INTO %(cur_table)s ("
    s1 = ", ".join(["%s" % prop[0] for prop in props if 'read' in prop[3]])
    s2 = ")\nSELECT '0', "
    s3 = ", ".join(["%s" % prop[0] for prop in props if 'read' in prop[3] and 'is_champ' not in prop[0]])
    s4 = " FROM %(eu_table)s\nINNER JOIN\n(SELECT team1 as team FROM %(cur_table)s WHERE is_champ=1 GROUP BY team1) t" \
         "\nON %(eu_table)s.team1=t.team OR %(eu_table)s.team2=t.team"

    """
        INSERT INTO %(cur_table)s (is_champ, date, team1, team2, goals1, goals2, result, result01,
    cof1, cofX, cof2)
        SELECT '0', date, team1, team2, goals1, goals2, result, result01,
    cof1, cofX, cof2 FROM %(eu_table)s
        INNER JOIN
        ( SELECT team1 as team FROM %(cur_table)s WHERE is_champ=1 GROUP BY team1) t
        ON %(eu_table)s.team1=t.team OR %(eu_table)s.team2=t.team
    """

    return s0+s1+s2+s3+s4


def update_query(props):
    s0 = "UPDATE %(tableName)s SET\n"
    s1 = ",\n".join(["{name}=%({name}){format}".format(name=prop[0], format=prop[2])
                     for prop in props if 'update' in prop[3]])
    s2 = "\nWHERE date='%(date)s' AND team1='%(team1)s' AND team2='%(team2)s'"

    return s0+s1+s2


def update_dict(props):
    return {prop[0]: prop[4] for prop in props if 'update' in prop[3]}


def headers_inds(headers, props):
    p_headers = [prop[0] for prop in props]
    return [p_headers.index(h) for h in headers]


def formats_to_write(inds, props):
    return '%' + '\t%'.join([props[i][2] for i in inds]) + '\n'


def format_row_txt(row, inds, props):
    return [props[i][5](row[i+1]) for i in inds]


def format_row_xls(row, inds, props):
    return [props[i][6](row[i+1]) for i in inds]


def odd(x):
    return x if x % 2 == 1 else x+1
