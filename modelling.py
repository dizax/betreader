# -*- coding: utf-8 -*-
__author__ = 'zax'

import time
import datetime
import argparse
import args_proc as a
from model_optimizer import ModelOptimizer
from model_d_optimizer import ModelDrawOptimizer
from model_tri_optimizer import ModelTriOptimizer

parser = argparse.ArgumentParser()
parser.add_argument('--all', action='store_true', help="Clear all and start over")
parser.add_argument('--update', action='store_true', help="Update new seasons from existing sets")
parser.add_argument('--add', action='store_true', help="Add new sets")

options = vars(parser.parse_args())
m_all = options['all']
m_update = options['update']
m_add = options['add']

if m_all:
    if not a.query_yes_no('This will drop the database. Are you sure?'):
        exit(0)

# init
optimizer = ModelOptimizer()
#optimizer = ModelDrawOptimizer()
#optimizer = ModelTriOptimizer()

if m_all:
    print "Start: ---", datetime.datetime.now(), "---\n"
    start_time = time.time()
    optimizer.optimize_new_data()
    print("--- optimizing done in %s seconds ---" % (time.time() - start_time))
elif m_update:
    print "Start: ---", datetime.datetime.now(), "---\n"
    start_time = time.time()
    optimizer.optimize_update_sets()
    print("--- optimizing done in %s seconds ---" % (time.time() - start_time))
elif m_add:
    print "Start: ---", datetime.datetime.now(), "---\n"
    start_time = time.time()
    optimizer.optimize_add_sets()
    print("--- optimizing done in %s seconds ---" % (time.time() - start_time))
else:
    print "No options specified"

optimizer.disconnect()

