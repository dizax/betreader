# -*- coding: utf-8 -*-
__author__ = 'zax'

import sys


def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning
        an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True,
             "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = raw_input().lower()
        if default is not None and choice == '':
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' "
                             "(or 'y' or 'n').\n")


def process_options(options):
    #read_last = options['readLast']
    read_all = options['readAll']
    read_backup = options['readAllBackup']
    #proc_last = options['procLast']
    proc_all = options['procAll']
    #write_last = options['writeLast']
    write_all = options['writeAll']

    read = options['readLast'] or options['readAll']
    process = options['procLast'] or options['procAll']
    write = options['writeLast'] or options['writeAll']
    model = options['model']

    if model:
        read = False
        process = False
        write = False
        read_all = False
        read_backup = False
        proc_all = False
        write_all = False
    if write:
        read = False
        process = False
        model = True
        read_all = False
        read_backup = False
        proc_all = False
        #write_all = False
    if process:
        read = False
        write = True
        model = True
        read_all = False
        read_backup = False
        #proc_all = False
        write_all = proc_all or write_all
    if read:
        process = True
        write = True
        model = True
        read_all = True if read_backup else read_all
        proc_all = read_all or proc_all
        write_all = read_all or write_all
    if not read and not process and not write and not model:
        read = True
        process = True
        write = True
        model = True
        read_all = False
        read_backup = False
        proc_all = read_all or proc_all
        write_all = read_all or write_all

    return read, read_all, read_backup, process, proc_all, write, write_all, model
