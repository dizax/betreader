# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
from model_base_optimizer import ModelBaseOptimizer
import dyn_query as qry
import const


class ModelTriOptimizer(ModelBaseOptimizer):

    db_name = 'optimize_tri.db'

    def get_row_data(self, data, res, row, w_cnt, cur_week_min, s):
        country = row[0].encode('utf-8')
        league = row[1].encode('utf-8')
        season = row[2].encode('utf-8')
        table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

        # -- <data interval ---------------------------------
        weeks = self.weeks(table_name)

        week_window = (qry.odd(int(len(weeks) / self.sets[s][1])) - 1) / 2

        week_min_i = max(w_cnt - week_window, 0)
        if week_min_i + 2*week_window < len(weeks):
            week_max_i = week_min_i + 2*week_window
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, cur_week_min, weeks[week_min_i], weeks[week_max_i])
        else:
            week_min_i = max(len(weeks)-1 - 2*week_window, 0)
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, cur_week_min, weeks[week_min_i])
        # -- /data interval> ---------------------------------

        self.cur_origin.execute(sql)
        results = self.cur_origin.fetchall()

        for row in results:
            res.append(int(row[7]))
            data.append(qry.format_row_xls(row, self.sets[s][0], const.headers))

    def week_result(self, model, table_name, w_cnt, weeks, s):
        # games list
        week_min = weeks[max(w_cnt - self.weeks_cnt, 0)]
        if w_cnt + self.weeks_cnt + 1 < len(weeks):
            week_max = weeks[w_cnt + self.weeks_cnt + 1]
            #weeks_cnt = (w_cnt + self.weeks_cnt + 1) - max(w_cnt - self.weeks_cnt, 0)

            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, week_min, week_max)
        else:
            #weeks_cnt = len(weeks) - max(w_cnt - self.weeks_cnt, 0)

            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, week_min)

        profit, profit_str, guess, guess_str = 0., 0., 0, 0
        try:
            self.cur_origin.execute(sql)
            results = self.cur_origin.fetchall()
            
            games_cnt = len(results)

            for r, row in enumerate(results):
                #date = row[2].strftime('%Y-%m-%d')
                #date = row[2].encode('utf-8')
                res = int(row[7])
                c = [float(row[9]), float(row[10]), float(row[11])]

                test_data = [qry.format_row_xls(row, self.sets[s][0], const.headers)]

                probs = model.predict_proba(test_data)

                try:
                    e_ = [probs[0][2] * c[0], probs[0][1] * c[1], probs[0][0] * c[2]]
                    e_bet = e_.index(max(e_))
                    s_ = [probs[0][2], probs[0][1], probs[0][0]]
                    s_bet = s_.index(max(s_))

                    profit += (c[e_bet]-1. if -res+1 == e_bet else -1.)
                    profit_str += (c[s_bet]-1. if -res+1 == s_bet else -1.)
                    guess += int(-res+1 == e_bet)
                    guess_str += int(-res+1 == s_bet)
                except IndexError:
                    continue
        except mdb.Error:
            print "Error: unable to fetch data for modeling"
            return (0., 0., 0., 0.)

        return (profit / float(games_cnt), profit_str / float(games_cnt),
                float(guess) / float(games_cnt), float(guess_str) / float(games_cnt))