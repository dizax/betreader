# -*- coding: utf-8 -*-
__author__ = 'zax'

from bs4 import BeautifulSoup
import re
from selenium import webdriver
import selenium.common.exceptions as se
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.common.by import By
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from datetime import datetime


class BaseReader:

    def __init__(self):
        self.br = self.new_driver()
        #self.wait = WebDriverWait(self.br, 10)

    def close(self):
        self.br.quit()

    @staticmethod
    def new_driver():
        #cookie_file_path = 'cookie.txt'
        #args = ['--cookies-file={}'.format(cookie_file_path)]
        args = ['--load-images=no']

        user_agent = (
            "Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 " +
            "(KHTML, like Gecko) Chrome/41.0.2272.118 Safari/537.36"
        )
        d_cap = dict(DesiredCapabilities.PHANTOMJS)
        d_cap["phantomjs.page.settings.userAgent"] = user_agent
        
        driver = webdriver.PhantomJS(service_args=args, desired_capabilities=d_cap)

        return driver

    def get_soup_from_driver(self, m_link):
        response = ''
        wait = WebDriverWait(self.br, 10)
        css_selector = "div#tournamentTable" if 'results' in m_link else "table#tournamentTable"
        while True:
            try:
                self.br.get(m_link)
                wait.until(ec.visibility_of_element_located(
                    (By.CSS_SELECTOR, css_selector)))
                response = self.br.page_source.encode('utf-8')
                break
            except se.TimeoutException:
                print 'Timeout exception', m_link
                continue

        #f = file(m_link[40:].replace('/', '_') + '.html', 'w')
        #f.write(response)
        #f.close()
        return BeautifulSoup(response)

    def get_soup_from_driver_js(self, m_link, m_js):
        response = ''
        wait = WebDriverWait(self.br, 10)
        css_selector = "div#tournamentTable" if 'results' in m_link else "table#tournamentTable"
        while True:
            try:
                self.br.get(m_link)
                self.br.execute_script(m_js)
                wait.until(ec.visibility_of_element_located(
                    (By.CSS_SELECTOR, css_selector)))
                response = self.br.page_source.encode('utf-8')
                break
            except se.TimeoutException:
                print 'Timeout exception js', m_link
                continue

        return BeautifulSoup(response)
    
    @staticmethod
    def upcoming_url(country, league):
        return 'http://www.oddsportal.com/soccer/' + country + '/' + league

    @staticmethod
    def league_url(country, league):
        return 'http://www.oddsportal.com/soccer/' + country + '/' + league + '/results'

    def seasons(self, country, league):
        soup = self.get_soup_from_driver(self.league_url(country, league))

        seasons = soup.find_all('ul', 'main-filter')[1].find_all('a')
        # limit seasons cnt
        #seasons = seasons[1:2]

        return [[re.findall('(\d{4}/\d{4}|(?<!/)\d{4}(?!/))', s.text.encode('utf-8'))[0].replace('/', '-'),
                'http://www.oddsportal.com'+s['href']] for s in seasons]

    def pages(self, season_url):
        soup = self.get_soup_from_driver(season_url)

        pages = soup.find_all('div', id='pagination')

        if len(pages) == 0:
            return [season_url]
        else:
            return [season_url] + [season_url+a['href'] for a in pages[0].find_all('a')[2:-2]]

    @staticmethod
    def format_html(m_text):
        m_text = re.sub('<[^>]*>', '', m_text)
        m_text = re.sub(' +\s+', '\n', m_text)
        m_text = re.sub('\n+', '\n', m_text)
        m_text = m_text.replace('\n:', ':')
        m_text = m_text.replace('Примечания', '\nПримечания')
        return m_text.replace('Подсказка', '\nПодсказка')

    @staticmethod
    def format_date(m_date):
        # return datetime.strptime(m_date, '%d %b %Y').strftime('%Y-%m-%d')
        return datetime.fromtimestamp(int(m_date)).strftime('%Y-%m-%d')

    @staticmethod
    def find_between(s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""

    @staticmethod
    def find_before(s1, s2):
        try:
            return s1[:s1.index(s2)]
        except ValueError:
            return ""

    @staticmethod
    def find_after(s1, s2):
        try:
            return s1[s1.index(s2) + len(s2):]
        except ValueError:
            return ""

    @staticmethod
    def result(g1, g2):
        if g1 > g2:
            return 1
        if g1 < g2:
            return -1
        return 0
