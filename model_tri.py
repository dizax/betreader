# -*- coding: utf-8 -*-
__author__ = 'zax'

from datetime import datetime
try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
from model_base import ModelBase
import dyn_query as qry
import const


class ModelTri(ModelBase):

    db_name = 'optimize_tri.db'
    xls_name = 'model_tri'

    def fit_league_data(self, color, ws, table_name, country, league, season, mw_cnt):
        headers = ['date', 'team1', 'team2', 'goals1', 'goals2', 'result', 'c_cof1', 'c_cofX', 'c_cof2',
                   'set_id', 'm_cof1', 'm_cofX', 'm_cof2', 'E1', 'EX', 'E2']
        for c, h in enumerate(headers):
            ws.write(0, c, h.decode('utf-8'))

        # first dates of match weeks
        weeks = self.weeks(table_name)

        # predict week results
        profit, profit_str, guess, guess_str,  r_cnt = 0., 0., 0, 0, 0
        w_profit = []
        for w_cnt, (week, week_next) in reversed(list(enumerate(zip(weeks, weeks[1:] + [None])))):
            m_profit, m_profit_str, m_guess, m_guess_str, m_rcnt = self.fit_week_data(color, ws, r_cnt, table_name,
                                                                                      country, league, season, mw_cnt,
                                                                                      w_cnt, week, week_next)
            profit += m_profit
            profit_str += m_profit_str
            guess += m_guess
            guess_str += m_guess_str
            r_cnt += m_rcnt[2]

            w_profit.append(m_profit)

        # cumulative w_profits
        w_profit = [sum(w_profit[i:]) for i, wp in reversed(list(enumerate(w_profit)))]

        # write profit results
        ws.write(0, len(headers)+4, "profit")
        ws.write(1, len(headers)+4, profit)
        ws.write(0, len(headers)+5, "profit_str")
        ws.write(1, len(headers)+5, profit_str)
        ws.write(0, len(headers)+6, "guess")
        ws.write(1, len(headers)+6, float(guess)/float(r_cnt))
        ws.write(0, len(headers)+7, "guess_str")
        ws.write(1, len(headers)+7, float(guess_str)/float(r_cnt))

        return profit, w_profit

    def predict_result(self, table_name, week, week_next, best_set_id, best_set, log_reg, ws, r_cnt, color):
        # games list
        if week_next is not None:
            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, week, week_next)
        else:
            sql = """SELECT * FROM %s
                WHERE is_champ=1
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, week)

        # predict
        try:
            self.db.cur.execute(sql)
            results = self.db.cur.fetchall()

            profit, profit_str, guess, guess_str = 0., 0., 0, 0
            for r, row in enumerate(results):
                #date = row[2].strftime('%Y-%m-%d')
                date = row[2].encode('utf-8')
                c1 = float(row[9])
                cx = float(row[10])
                c2 = float(row[11])
                c_ = [c1, cx, c2]

                data_to_write = [
                    row[2],  # 0 date
                    row[3],  # 1 team1
                    row[4],  # 2 team2
                    int(row[5]),  # 3 goals1
                    int(row[6]),  # 4 goals2
                    int(row[7]),  # 5 result
                    c1,  # 6 c_cof1
                    cx,  # 7 c_cofX
                    c2,  # 8 c_cof2
                    best_set_id  # 9 set_id
                ]

                test_data = [qry.format_row_xls(row, best_set[:-1], const.headers)]

                #results = log_reg.predict(test_data)
                probs = log_reg.predict_proba(test_data)

                try:
                    data_to_write.append(probs[0][2])  # 10 m_cof1
                    data_to_write.append(probs[0][1])  # 11 m_cofX
                    data_to_write.append(probs[0][0])  # 12 m_cof2
                    data_to_write.append(probs[0][2]*c1)  # 13 E1
                    data_to_write.append(probs[0][1]*cx)  # 14 EX
                    data_to_write.append(probs[0][0]*c2)  # 15 E2

                    e = data_to_write[13:]
                    e_bet = e.index(max(e))
                    s = [probs[0][2], probs[0][1], probs[0][0]]
                    s_bet = s.index(max(s))
                    data_to_write.append([row[3], "draw", row[4]][e_bet])

                    data_to_write.append(c_[e_bet]-1. if -int(row[7])+1 == e_bet else -1.)  # profit m_cof*E
                    data_to_write.append(c_[s_bet]-1. if -int(row[7])+1 == s_bet else -1.)  # profit straight
                except IndexError:
                    continue

                if (datetime.now() - datetime.strptime(date, '%Y-%m-%d')).days < 1:
                    for c, col in enumerate(data_to_write):
                        ws.write(r_cnt + r+1, c, col, color)
                else:
                    for c, col in enumerate(data_to_write):
                        ws.write(r_cnt + r+1, c, col)
                    profit += data_to_write[-2]
                    profit_str += data_to_write[-1]
                    guess += int(-int(row[7])+1 == e_bet)
                    guess_str += int(-int(row[7])+1 == s_bet)

            return profit, profit_str, guess, guess_str, (0, 0, len(results))
        except mdb.Error:
            print "Error: unable to fetch data for modeling"
            exit(0)

    def get_row_data(self, data, res, row, w_cnt, date, best_set):
        country = row[0].encode('utf-8')
        league = row[1].encode('utf-8')
        season = row[2].encode('utf-8')
        table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

        # -- data interval ---------------------------------
        weeks = self.weeks(table_name)
        week_window = (qry.odd(int(len(weeks) / best_set[-1])) - 1) / 2

        week_min_i = max(w_cnt - week_window, 0)
        if week_min_i + 2*week_window < len(weeks):
            week_max_i = week_min_i + 2*week_window
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s' AND date<'%s'
                ORDER BY date DESC""" % (table_name, date, weeks[week_min_i], weeks[week_max_i])
        else:
            week_min_i = max(len(weeks)-1 - 2*week_window, 0)
            sql = """SELECT * FROM %s
                WHERE is_champ=1 AND date < '%s'
                AND date>='%s'
                ORDER BY date DESC""" % (table_name, date, weeks[week_min_i])
        # -- data interval ---------------------------------

        self.db.cur.execute(sql)
        results = self.db.cur.fetchall()

        for row in results:
            res.append(int(row[7]))
            data.append(qry.format_row_xls(row, best_set[:-1], const.headers))
