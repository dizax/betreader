# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
import const
import dyn_query as qry


class Database:

    m_cur_table = ''

    def __init__(self, init=None, read_all=False):
        if init is None:
            return

        self.read_all = read_all
        drop_seasons = read_all

        # connect to db
        self.con = mdb.connect('bet.db')
        self.cur = self.con.cursor()

        self.insert_qry = qry.insert_query([prop for prop in const.headers if 'read' in prop[3]])
        self.insert_eu_qry = qry.insert_eu_query(const.headers)

        self.cur.execute("SELECT SQLITE_VERSION()")
        data = self.cur.fetchone()
        print "Database version : %s " % data

        # drop if needed
        if drop_seasons:
            print 'Dropping Seasons_list'
            self.drop_seasons()

        # create table (if don't exist?)
        sql = """CREATE TABLE IF NOT EXISTS Seasons_list (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                country TEXT NOT NULL,
                league TEXT NOT NULL,
                season TEXT NOT NULL
            )"""

        self.cur.execute(sql)

    def drop_seasons(self):
        sql = """SELECT country, league, season
            FROM Seasons_list"""
        try:
            self.cur.execute(sql)
            results = self.cur.fetchall()

            for row in results:
                country = row[0].encode('utf-8')
                league = row[1].encode('utf-8')
                season = row[2].encode('utf-8')
                table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

                self.drop_season(table_name)
        except mdb.Error:
            print "Seasons_list not exists"
            return -1

        self.cur.execute("DROP TABLE IF EXISTS Seasons_list")
        return 1

    def drop_season(self, table_name):
        self.cur.execute("DROP TABLE IF EXISTS %s" % table_name)

    def disconnect(self):
        self.cur.close()
        self.con.close()

    def add_season(self, country, league, season):
        self.m_cur_table = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')

        print "adding table " + self.m_cur_table

        equals = []
        # look for season in seasons list
        sql = """SELECT * FROM Seasons_list
            WHERE country='%(country)s' AND league='%(league)s' AND season='%(season)s'
            """ % {"country": country, "league": league, "season": season}

        try:
            self.cur.execute(sql)
            equals = self.cur.fetchall()
        except mdb.Error:
            print "cant read records from seasons table"
            self.con.rollback()

        # add to seasons list
        if len(equals) == 0:
            sql = """INSERT INTO Seasons_list(country, league, season)
                VALUES ('%(country)s', '%(league)s', '%(season)s')
                """ % {"country": country, "league": league, "season": season}
            try:
                self.cur.execute(sql)
                self.con.commit()
            except mdb.Error:
                print "cant add record to seasons table"
                self.con.rollback()

        # create new table
        self.cur.execute("DROP TABLE IF EXISTS " + self.m_cur_table)

        # create table (if don't exist?)
        create_sql = qry.create_query(const.headers) % self.m_cur_table

        self.cur.execute(create_sql)

        return 0

    def add_match(self, ins_d):
        if not ins_d['is_champ']:
            sql = """SELECT team1
                FROM %s
                WHERE is_champ=1
                GROUP BY team1""" % self.m_cur_table
            try:
                self.cur.execute(sql)
                results = self.cur.fetchall()
                teams = [r[0].encode('utf-8') for r in results]
                if ins_d['team1'] not in teams and ins_d['team2'] not in teams:
                    return
            except mdb.Error:
                print "Error: unable to teams"
                exit(0)

        # add to seasons list
        ins_d['tableName'] = self.m_cur_table
        sql = self.insert_qry % ins_d

        try:
            self.cur.execute(sql)
            self.con.commit()
        except mdb.Error:
            print "cant add record to match table"
            self.con.rollback()

        return 0

    def add_europa_matches(self, country, league, season):
        sql1 = """SELECT league
            FROM Seasons_list
            WHERE country='%s' AND season='%s'""" % ('europe', season)
        try:
            self.cur.execute(sql1)
            results = self.cur.fetchall()

            for row in results:
                league = row[0].encode('utf-8')
                europa_table = 'europe_' + league.replace('-', '') + '_' + season.replace('-', '')

                sql2 = self.insert_eu_qry % {'cur_table': self.m_cur_table, 'eu_table': europa_table}

                self.cur.execute(sql2)
        except mdb.Error:
            print "Error: unable to add europa matches"
            exit(0)
