# -*- coding: utf-8 -*-
__author__ = 'zax'

#from bs4 import BeautifulSoup
import re
from reader_base import BaseReader
from database import Database
import const
import dyn_query as qry


class BetReader(BaseReader):

    def __init__(self, read_all):
        BaseReader.__init__(self)

        self.LEAGUES = const.LEAGUES
        self.SUB_LEAGUES = const.SUB_LEAGUES

        self.db = Database()
        self.br = self.new_driver()

        self.read_all = read_all

        self.ins_d = qry.insert_dict(const.headers)
        self.p = re.compile(ur'\s+\([a-zA-Z ]{1,3}\)\s*$', re.UNICODE)

    def read(self, db):
        # set database
        self.db = db

        # read
        for (country, league) in self.LEAGUES:
            self.read_country(country, league)

        self.br.close()

    def read_country(self, country, league):
        print 'reading %s %s' % (country, league)

        js = 'changeOddsFormat(1); return false;'

        for i, season_d in enumerate(self.seasons(country, league)):
            print season_d[0]

            s_db = season_d[0]
            season = '-' + season_d[0] if i > 0 else ''

            season_url = season_d[1]
            soup = self.get_soup_from_driver(season_url)

            # process leagues season
            emptyMsg = soup.find('table', id='tournamentTable').find('tbody').find_all('td', id='emptyMsg')
            if len(emptyMsg) > 0:
                print 'empty table'
                if i == 0:
                    self.db.add_season(country, league, s_db)
                else:
                    continue
            else:
                self.db.add_season(country, league, s_db)

                # check for table exists
                for page_url in self.pages(season_url):
                    soup = self.get_soup_from_driver(page_url)

                    while not self.read_table(soup, False, 'europe' in country, False):
                        soup = self.get_soup_from_driver_js(page_url, js)

            # process subleagues season
            if 'europe' not in country:
                try:
                    for subleague in self.SUB_LEAGUES[country]:
                        self.read_subleague_season(country, subleague, season, s_db)
                except KeyError:
                    print 'no subleagues for ' + country
                self.db.add_europa_matches(country, league, s_db)

            # process upcoming matches
            if i == 0:
                self.read_league_upcoming(country, league)
                # skip previous seasons
                if not self.read_all:
                    break

    def read_league_season(self, country, league, season, s_db):
        return

    def read_subleague_season(self, country, subleague, season, s_db):
        seasons = self.seasons(country, subleague)
        s_names = [s[0] for s in seasons]
        
        if s_db not in s_names:
            return

        print 'reading %s %s' % (country, subleague)
        js = 'changeOddsFormat(1); return false;'

        season_url = seasons[s_names.index(s_db)][1]
        soup = self.get_soup_from_driver(season_url)

        emptyMsg = soup.find('table', id='tournamentTable').find('tbody').find_all('td', id='emptyMsg')
        if len(emptyMsg) > 0:
            print 'empty table'
            return

        # check for table exists
        for page_url in self.pages(season_url):
            soup = self.get_soup_from_driver(page_url)

            while not self.read_table(soup, False, False, True):
                soup = self.get_soup_from_driver_js(page_url, js)

    def read_league_upcoming(self, country, league):
        print 'reading upcoming %s %s' % (country, league)

        js = 'changeOddsFormat(1); return false;'

        soup = self.get_soup_from_driver(self.upcoming_url(country, league))

        emptyMsg = soup.find('table', id='tournamentTable').find('tbody').find_all('td', id='emptyMsg')
        if len(emptyMsg) > 0:
            print 'empty table'
            return

        # check for table exists
        while not self.read_table(soup, True, False, False):
            soup = self.get_soup_from_driver_js(self.upcoming_url(country, league), js)

    def read_table(self, m_soup, upcoming, eu, sub):
        date = ''
        league_match = True

        odds_format = m_soup.find('a', 'user-header-fakeselect', id='user-header-oddsformat-expander')
        try:
            if 'EU' not in odds_format.text:
                print "Err:" + odds_format.text
                return False
        except AttributeError:
            print "NonType oddsFormat"
            return False

        try:
            m_table = m_soup.find('table', id='tournamentTable').find('tbody')
        except (NameError, TypeError):
            print "Table not found"
            return False

        for i, row in enumerate(m_table.find_all('tr')):
            class_name = ' '.join(row['class']).encode('utf-8') if row.has_attr('class') else ''

            if 'dark' in class_name or 'dummy' in class_name:
                continue

            # date
            if 'border' in class_name:
                date_text = row.find_all('th')[0].text
                date_text = re.sub('\d{2} [A-Za-z]{3} \d{4}', '', date_text)
                league_match = ('' == date_text or 'Yesterday' in date_text or eu)

                tmp = ' '.join(row.find('span')['class']).encode('utf-8').replace('datet t', '').replace('-2-0-0-1', '')
                date = self.format_date(tmp)
                continue

            # ignore nonleague matches
            if not upcoming:
                if not league_match:
                    continue

            # match
            if not self.read_row(row, date, upcoming, sub):
                return False

        return True

    def read_row(self, m_row, m_date, upcoming, sub):
        ins_d = self.ins_d
        ins_d['date'] = m_date
        ins_d['is_champ'] = not sub

        cols = m_row.find_all('td')

        # participants
        a_teams = cols[1].find_all('a')[-1] if upcoming else cols[1]
        winner = a_teams.find('span', {"class": "bold"})
        teams = a_teams.text
        team1 = self.p.sub('', self.find_before(teams, ' - '))
        team2 = self.p.sub('', self.find_after(teams, ' - '))
        #team1 = re.sub(ur'\s+\([a-zA-Z ]{1,3}\)\s*$', '', self.find_before(teams, ' - '))
        #team2 = re.sub(ur'\s+\([a-zA-Z ]{1,3}\)\s*$', '', self.find_after(teams, ' - '))

        ins_d['team1'] = team1  # self.find_before(teams, ' - ')
        ins_d['team2'] = team2  # self.find_after(teams, ' - ')

        # result
        goals = cols[2].text
        goals1, goals2 = 0, 0
        goals_ = re.sub('[^\d:]', '', goals)
        
        try:
            if 'pen' in goals:
                # count as draw
                pass
            elif 'award' in goals:
                if winner is not None:
                    goals1 = 3 if winner.text in team1 else 0
                    goals2 = 3 if winner.text in team2 else 0
            else:
                goals1 = int(self.find_before(goals_, ':'))
                goals2 = int(self.find_after(goals_, ':'))
        except ValueError:
            #print "Bad goals: %s %s %s : %s" % (m_date, team1, team2, goals)
            if winner is not None:
                goals1 = 1 if winner.text in team1 else 0
                goals2 = 1 if winner.text in team2 else 0

        ins_d['goals1'] = goals1
        ins_d['goals2'] = goals2
        ins_d['result'] = self.result(goals1, goals2)
        ins_d['result01'] = int(ins_d['result'] == 1)
        
        clen = len(cols)
        # win 1
        try:
            ins_d['cof1'] = float(cols[clen-4].text)
            ins_d['p1'] = 1. / ins_d['cof1']
        except ValueError:
            #print "Bad cof: %s %s %s : %s" % (m_date, team1, team2, cols[3].text)
            return '-' in cols[clen-4].text
        except AttributeError:
            #print "NonType: %s %s %s" % (m_date, team1, team2)
            ins_d['cof1'] = 0.
            ins_d['p1'] = 0.

        # X
        try:
            ins_d['cofX'] = float(cols[clen-3].text)
            ins_d['pX'] = 1. / ins_d['cofX']
        except ValueError:
            #print "Bad cof:%s %s %s : %s" % (m_date, team1, team2, cols[4].text)
            return '-' in cols[clen-3].text
        except AttributeError:
            #print "NonType: %s %s %s" % (m_date, team1, team2)
            ins_d['cofX'] = 0.
            ins_d['pX'] = 0.

        # win 2
        try:
            ins_d['cof2'] = float(cols[clen-2].text)
            ins_d['p2'] = 1. / ins_d['cof2']
        except ValueError:
            #print "Bad cof:%s %s %s : %s" % (m_date, team1, team2, cols[5].text)
            return '-' in cols[clen-2].text
        except AttributeError:
            #print "NonType: %s %s %s" % (m_date, team1, team2)
            ins_d['cof2'] = 0.
            ins_d['p2'] = 0.

        self.db.add_match(ins_d)

        return True
