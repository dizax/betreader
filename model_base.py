# -*- coding: utf-8 -*-
__author__ = 'zax'

try:
    #import MySQLdb as mdb
    import sqlite3 as mdb
except ImportError:
    import pymysql as mdb
import xlsxwriter
from sklearn.linear_model import LogisticRegression
import const


class ModelBase:

    db_name = 'optimize.db'
    xls_name = 'model'

    def __init__(self, db):
        self.db = db

        self.con_set = None
        self.cur_set = None

        self.header_names = [h[0] for h in const.headers]
        self.sets_cols = []

    def fit_seasons(self):
        print "fitting seasons"

        for ssn in const.model_seasons_to_fit:
            print ssn
            self.fit_data(ssn)

    def fit_data(self, ssn=None):
        print 'fitting data'

        self.con_set = mdb.connect(self.db_name)
        self.cur_set = self.con_set.cursor()

        sql_cols = "PRAGMA table_info('Sets_list')"
        self.cur_set.execute(sql_cols)
        self.sets_cols = [row[1].encode("utf-8") for row in self.cur_set.fetchall()]

        wb = xlsxwriter.Workbook(self.xls_name+'.xlsx') if ssn is None \
            else xlsxwriter.Workbook(self.xls_name+'%s.xlsx' % ssn)
        color = [wb.add_format({'bg_color': 'yellow'}), wb.add_format({'bg_color': '#ffd800'})]

        if ssn is None:
            sql = """SELECT country, league, max(season) as ssn
                    FROM Seasons_list
                    WHERE country!='europe'
                    GROUP BY country, league"""
        else:
            sql = """SELECT country, league, season
                    FROM Seasons_list
                    WHERE country!='europe' AND season='%s'
                    GROUP BY country, league""" % ssn

        ws_sum = wb.add_worksheet('summary')
        cur_summary_str = 0
        for mw_cnt in const.model_weeks_cnt:
            countries, profits, w_profits = [], [], []

            try:
                self.db.cur.execute(sql)
                results = self.db.cur.fetchall()

                for row in results:
                    country = row[0].encode('utf-8')
                    league = row[1].encode('utf-8')
                    season = row[2].encode('utf-8')
                    table_name = country + '_' + league.replace('-', '') + '_' + season.replace('-', '')
                    ws = wb.add_worksheet(country + str(mw_cnt))

                    profit, w_profit = self.fit_league_data(color, ws, table_name, country, league, season, mw_cnt)

                    countries.append(country)
                    profits.append(profit)
                    w_profits.append(w_profit)
            except mdb.Error:
                print "Error: unable to fetch Seasons_list data for modeling"
                exit(0)

            # profit summary
            for j in xrange(max([len(wp) for wp in w_profits])):
                ws_sum.write(cur_summary_str, 3+j, j+1)
            cur_summary_str += 1
            for i, (c, p, wp) in enumerate(zip(countries, profits, w_profits)):
                ws_sum.write(cur_summary_str, 0, c + str(mw_cnt))
                ws_sum.write(cur_summary_str, 1, p)
                # cumulatives
                for j, wpp in enumerate(wp):
                    ws_sum.write(cur_summary_str, 3+j, wpp)
                cur_summary_str += 1
            cur_summary_str += 1

        wb.close()
        self.cur_set.close()
        self.con_set.close()

    def fit_league_data(self, color, ws, table_name, country, league, season, mw_cnt):
        raise NotImplementedError("Subclass must implement abstract method")

    def fit_week_data(self, color, ws, r_cnt, table_name, country, league, season, mw_cnt,
                      w_cnt, week, week_next):
        # compute best model (best parameters set) for the week
        # TODO (let the best set be the one that makes best result in the given week)
        sql1 = """SELECT set_id, AVG(guess)/(AVG(guess*guess) - AVG(guess)*AVG(guess)) as r
                FROM Optimizer_list
                WHERE country='%s' AND week>%d-1 AND week<%d+1 AND season<'%s'
                GROUP BY set_id ORDER BY r DESC;""" % (country, w_cnt-mw_cnt, w_cnt+mw_cnt, season)
        sql2 = """SELECT set_id, AVG(guess_str)/(AVG(guess_str*guess) - AVG(guess_str)*AVG(guess_str)) as r
                FROM Optimizer_list
                WHERE country='%s' AND week>%d-1 AND week<%d+1 AND season<'%s'
                GROUP BY set_id ORDER BY r DESC;""" % (country, w_cnt-mw_cnt, w_cnt+mw_cnt, season)
        sql3 = """SELECT set_id, AVG(result)/(AVG(result*result) - AVG(result)*AVG(result)) as r
                FROM Optimizer_list
                WHERE country='%s' AND week>%d-1 AND week<%d+1 AND season<'%s'
                GROUP BY set_id ORDER BY r DESC;""" % (country, w_cnt-mw_cnt, w_cnt+mw_cnt, season)
        sql4 = """SELECT set_id, AVG(res_str)/(AVG(res_str*res_str) - AVG(res_str)*AVG(res_str)) as r
                FROM Optimizer_list
                WHERE country='%s' AND week>%d-1 AND week<%d+1 AND season<'%s'
                GROUP BY set_id ORDER BY r DESC;""" % (country, w_cnt-mw_cnt, w_cnt+mw_cnt, season)
        self.cur_set.execute(sql3)
        best_set_id = int(self.cur_set.fetchall()[0][0])  # id of best set
        #best_set_id = 1  # id of best set

        # indexes of headers from best set
        sql_set = """SELECT * FROM Sets_list
                WHERE id=%d""" % best_set_id
        self.cur_set.execute(sql_set)
        res = self.cur_set.fetchall()
        best_set = []
        for i, row in enumerate(res[0][1:-2]):
            if int(row) == 1:
                try:
                    best_set.append(self.header_names.index(self.sets_cols[i+1]))
                except ValueError:
                    continue
        best_set.append(float(res[0][-2]))
        best_set.append(int(res[0][-1]))

        # build model
        x, y = self.get_data(country, league, week, 0, const.model_prev_seasons_cnt, w_cnt, best_set)
        log_reg = LogisticRegression()
        try:
            log_reg.fit(x, y)
        except ValueError:
            print 'odni nuli/ebenitzi'
            return 0., 0., 0, 0, (0, 0, 0)

        # predict
        return self.predict_result(table_name, week, week_next, best_set_id, best_set, log_reg, ws, r_cnt, color)

    def predict_result(self, table_name, week, week_next, best_set_id, best_set, log_reg, ws, r_cnt, color):
        raise NotImplementedError("Subclass must implement abstract method")

    def get_data(self, country, league, date, s_num, s_lim, w_cnt, best_set):
        res = []
        data = []

        # select 's_lim' previous seasons
        sql1 = """
                SELECT country, league, season
                FROM Seasons_list s0
                WHERE season IN (
                    SELECT season
                    FROM Seasons_list s
                    WHERE s.country = s0.country
                    AND (SELECT COUNT(*)
                        FROM Seasons_list st
                        WHERE st.season >= s.season AND st.country = s.country) >= %d
                    AND (SELECT COUNT(*)
                        FROM Seasons_list st
                        WHERE st.season >= s.season AND st.country = s.country) < %d
                ) AND country='%s' AND league='%s';
                """ % (s_num+1, s_num+1+best_set[-1], country, league)

        try:
            self.db.cur.execute(sql1)
            results1 = self.db.cur.fetchall()

            for row1 in results1:
                self.get_row_data(data, res, row1, w_cnt, date, best_set)
        except mdb.Error:
            print "Error: unable to fetch week data for writing"
            exit(0)

        return data, res

    def get_row_data(self, data, res, row, w_cnt, date, best_set):
        raise NotImplementedError("Subclass must implement abstract method")

    def weeks(self, table_name):
        sql = "SELECT COUNT(*) as cnt FROM %s" % ("stand_" + table_name)
        self.db.cur.execute(sql)
        w_cnt = int(self.db.cur.fetchall()[0][0]) / 2

        sql1 = "SELECT date FROM %s WHERE is_champ=1 ORDER BY date" % table_name
        self.db.cur.execute(sql1)
        res = self.db.cur.fetchall()

        # return first dates of match weeks
        return [row[0].encode('utf-8') for i, row in enumerate(res) if i % w_cnt == 0]
